system("clear") or system("cls")
# well.
#  if user os is Linux, then system will 'clear'
# else.
#  if user os is Windows, then system will 'cls'
# end. :)

require 'socket'
require_relative 'lib/port_scanner'

system("ruby arm/logo.rb")		# im using system
								# to call the logo

while true
	printf("Target HOST           => ")
	x = gets.chomp
	puts "\nStarting RedEye 1.0"
	threads = []
	port_lis = [9,20,21,22,23,25,37,42,43,49,67,68,69,79,80,88,109,110,111,218,209,427,491,513,561,660,694,752,754,989,990,1241,1311,3306,4001,5000,5050,5985,1443,443,53,1443,135]
	port_lis.each { |i| 
		threads << Thread.new {
			scans(x,i)
		}
	}
	threads.each(&:join)
end